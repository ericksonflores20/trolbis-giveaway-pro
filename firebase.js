
const env = "pro";

let firebaseConfig = {}


let url = ""

if(env === "dev"){
  firebaseConfig = {
    apiKey: "AIzaSyAykjbCF474vacyG4Il1Au4EI9bee3F9HY",
    authDomain: "trolbis-giveaway.firebaseapp.com",
    projectId: "trolbis-giveaway",
    storageBucket: "trolbis-giveaway.appspot.com",
    messagingSenderId: "725704051152",
    databaseURL: "https://trolbis-giveaway-default-rtdb.firebaseio.com",
    appId: "1:725704051152:web:bfff1315e3576e8ec211d5",
    measurementId: "G-SYPCW46XX0"
  };
  url = "https://trolbis-backend.vercel.app";
}


if(env === "pro"){
  firebaseConfig = {
    apiKey: "AIzaSyD6NmXNiNHm69fBFzZEtKAilxoKdqTa7a0",
    authDomain: "trolbis-giveaway-pro.firebaseapp.com",
    projectId: "trolbis-giveaway-pro",
    storageBucket: "trolbis-giveaway-pro.appspot.com",
    messagingSenderId: "894291643554",
    databaseURL: "https://trolbis-giveaway-pro-default-rtdb.firebaseio.com",
    appId: "1:894291643554:web:702126472bc15c595cdbfb",
    measurementId: "G-0HBENM3LRZ"
  };
  url = "https://trolbis-giveaway.vercel.app/";
}


  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);