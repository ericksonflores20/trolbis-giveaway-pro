
const database = firebase.database();

let userExist = false;
let ipInfo = 0;
let timestamp = {nanoseconds: 0, seconds: 1562524200}
let userProfile = {};
let limit = 5;

 async function saveUser() {

    let form = document.getElementById('form');
    let data = {}
    let inputElements = form.querySelectorAll("input, textarea")


    for (const i in inputElements) {

       if(validateFields(inputElements[i].id)){
           document.getElementById(inputElements[i].id). style = "border: solid 1px red"
           return
       }

       if(document.getElementById(inputElements[i].id)){
        data[inputElements[i].id] = document.getElementById(inputElements[i].id).value.trim();
       }
       
    }
    let btn = document.querySelector('#btn-enviar')
    btn.disabled = true;
    btn.style = "background-color:grey;border:none"
    await valdiateUser(data.nombre);
    let ip =  await getIpInfo();
    let today =  ISODateString(new Date())

    if(!userExist){
        data.nombre = data.nombre.toLowerCase()
        data.votos = 0;
        data.ip =  ip.ip
        data.date =  today;
       let result =  await database.ref('/users/').push(data).then(()=>true).catch(() => false);

       if(result){
            swal(data.nombre, " Ya estas concursando con tu gran historia!", "success")
            .then(()=> window.location.href = 'final.html?user='+data.nombre)
       }
    }

    if(userExist){
        swal("Oops!", "Este usuario ya se encuentra registrado!", "warning");
        
    }

    btn.disabled = false;
    btn.style = "background-color:#1da0d5;border:none"
    
}


function validateFields(id) {
    let field = document.getElementById(id) && document.getElementById(id).value
    if(field == ' ' ||field == '' || field == null){
        document.getElementById(id+'-label') && document.getElementById(id+'-label').removeAttribute("hidden")
        return id
    }

    if(id =='telefono' && field.length<9){
        document.getElementById(id+'-label') && document.getElementById(id+'-label').removeAttribute("hidden")
        document.getElementById(id+'-label') && (document.getElementById(id+'-label').innerHTML = "El telefono esta incompleto! ")
        return id
    }
    document.getElementById(id+'-label') && (document.getElementById(id+'-label').hidden = true);
    document.getElementById(id) && ( document.getElementById(id).style = "");
    return false
    
}



 const  valdiateUser = (user) =>{
    let validate =  database.ref("users").orderByChild('nombre').equalTo(user);
    return validate.once("value", snapshot => { userExist = snapshot.exists()});
 } 


 const  valdiateIp = (user) =>{
    let validate =  database.ref("users").orderByChild('nombre').equalTo(user);
    return validate.once("value", snapshot => { userExist = snapshot.exists()});
 } 



 function validateNumber() {
     let number = document.getElementById("telefono");
 }


 async function vote(user) {
    let vote = parseInt(document.getElementById("count-"+user).innerHTML)
    let data =  await getIpInfo();
    let request = {
        ip:data.ip,
        user: user,
        date: ISODateString(new Date()),
        device: navigator.userAgent

    }
    let result = await database.ref('/votes/').push(request).then(()=>true).catch(() => false);
    window.localStorage.setItem("is_vote",'1')
    window.localStorage.setItem("vote_name",user)
    let votePlus = vote + 1;
    updateVote(user,votePlus)
    
 }


 async function updateVote(user, votePlus) {
   let ref =  database.ref('/users/').orderByChild("nombre").equalTo(user)
    ref.once("value", snapshot => { 
        if(snapshot.exists()){
            database.ref('/users/'+Object.keys(snapshot.val())[0]).update({votos: votePlus}).then(()=>
                document.getElementById("count-"+user).innerHTML = votePlus).catch(() => false
                
            );
        }
    })
 }



 async function voteExists(user = null) {
     user = user ?user : document.getElementById("user_id").innerHTML
    let isVote = window.localStorage.getItem("is_vote");
    if(isVote === '1'){
        swal("Ya votaste!", "Parece que has votado por una historia, no puedes repetir esta accion", "warning");
    }else{
        vote(user)
        swal("Bien votaste!", "has votado por "+user+" , gracias por tu apoyo!", "success")
        .then(()=> window.location.href = "historias.html");
    }
 }




 async function getUserDetails(user) {
    //linWs()
    document.getElementById("load").removeAttribute("hidden")
    document.getElementById("vote").id = "count-"+user
   let ref =  database.ref("users").orderByChild("nombre").equalTo(user);
    ref.once("value", snapshot => { 
        if(snapshot.exists()){
            let data = Object.values(snapshot.val())[0];
            document.getElementById("usuario").innerHTML = data.nombre.substring(0,2).toUpperCase();
            document.getElementById("story").innerHTML = data.comentario;
            document.getElementById("user_name").innerHTML = data.padre;
            document.getElementById("user_id").innerHTML = data.nombre;
            document.getElementById("count-"+user).innerHTML = data.votos
            document.getElementById("fecha").innerHTML = data.date.split(" ")[0];
            document.getElementById("hora").innerHTML = data.date.split(" ")[1];
            
        }
        document.getElementById("load").hidden = true;
    });
    
}


function htmlBody(element) {
    let html = '';
    html = ` <div class="conten_bola">
                <div class="bola2">
                    <p class="Usuario" id="usuario">${element.nombre.substring(0,2).toUpperCase()}</p>
                </div>

                <div class="conten_fecha_final">
                    <p class="nombre_final">${element.padre}</p>
                    <p class="fecha_final" id="user_id">${element.nombre}</p>
                </div>

            </div>
            

            <div class="conten_ver_Votar">
                <div class="conten_megusta">
                    <a class="${setVoteStyle(element.nombre)}" onclick="voteExists('${element.nombre}')">Votar</a>
                </div>
            </div>

            <div class="fondo">
                <p class="historias_final" style="width: 100%">${element.comentario.substring(0,200)}...<a class="ver_mas" href="final.html?user=${element.nombre}">VER MAS</a> </p>
            </div>

            <div class="conten_Gnl_final">
                <div class="conten_voto_final">
                    <p class="contador_final" id="count-${element.nombre}">${element.votos}</p>
                    <img class="corazon_final" src="./img/corazon.png" alt="">
                </div>
            </div>  `
            return html;
}

async function getUserOneCard(child,field, option = null) {
    let user = option ? option : document.getElementById("search").value;
    document.getElementById("load").removeAttribute("hidden")
    let ref =  database.ref(child).orderByChild(field).equalTo(user.toLowerCase());

     ref.once("value", snapshot => { 
        let container = document.getElementById("container");
        
        let html = '';

         if(snapshot.exists()){
            container.innerHTML = "";
             let element = Object.values(snapshot.val())[0];
              
             html += htmlBody(element);
             
            container.insertAdjacentHTML("beforeend", html);
         }else{
            swal("Oops!", "No hemos encontrado el usaurio", "warning");
         }
         
         document.getElementById("load") && (document.getElementById("load").hidden = true);
     });
}



function scrollEnd() {
   
}





async function getAllUser() {
   document.getElementById("load").removeAttribute("hidden")
   
    let ref =  database.ref("users").limitToFirst(5)
     ref.once("value", snapshot => { 
        let container = document.getElementById("container")
        let html = '';

        snapshot.forEach(element => {
            html += htmlBody(element.val());
        
        });
        container.insertAdjacentHTML("beforeend", html);
        document.getElementById("load").hidden = true;
     });
 }


 async function getVotesByUser(user) {
    let ref =  database.ref("votes").orderByChild("user").equalTo(user)
    ref.once("value", snapshot => { 
      

       let ip = Object.values(snapshot.val());

       for (const i in ip) {
       }
    });
 }


 async function changeLimitUser() {
     let field_limit = document.getElementById("limit");
     limit = parseInt(field_limit.value) + 5;
    document.getElementById("load").removeAttribute("hidden")
    
     let ref =  database.ref("users").limitToFirst(limit)
      ref.once("value", snapshot => { 
        let container = document.getElementById("container")
        deleteChild(container);
         let html = '';
 
         snapshot.forEach(element => {
             html += htmlBody(element.val());
         
         });
         container.insertAdjacentHTML("beforeend", html);
         document.getElementById("load").hidden = true;
      });
      field_limit.value = limit;
  }



function getMyVote() {
    let ref =  window.localStorage.getItem("vote_name");
        if(ref){
            getUserOneCard("users","nombre",ref)
        }else{
            swal("Oops!", "No hemos encontrado tu voto", "warning");
        }
     
 }


 function deleteChild(container) {
    while (container.firstChild) {
        container.removeChild(container.firstChild);
    }
 }


 function filterCard(asc) {
    document.getElementById("load").removeAttribute("hidden")
    let ref =  database.ref("users").orderByChild("votos")
     ref.once("value", snapshot => { 
        let container = document.getElementById("container")
        deleteChild(container);
        let html = '';
        let data = asc? 
        Object.values(snapshot.val()).sort((a, b) => parseFloat(a.votos) - parseFloat(b.votos)) 
        :
        Object.values(snapshot.val()).sort((a, b) => parseFloat(b.votos) - parseFloat(a.votos))
        for (const key in data) {
                html += htmlBody(data[key]);
                
        }
        container.insertAdjacentHTML("beforeend", html);
        document.getElementById("load").hidden = true;
     });
     
 }


 function countStory() {
    let ref =  database.ref("users").orderByChild("nombre")
     ref.once("value", snapshot => { 
        let data = Object.values(snapshot.val()).length

        document.getElementById("cont1").innerHTML = data;
        
     });
 }

 function changeFilter() {
     let filter = document.getElementById("filter")
     let data  = filter.value === '1'? false: true;
     filter.value = data? 1: 0;
     filterCard(data)
 }



 function setVoteStyle(user) {
    let ref =  window.localStorage.getItem("vote_name");

    if(ref === user){
        return 'votar_hiden'
    }

    if(ref !== user){
        return 'votar_historia'
    }
     
 }

 
 async function getIpInfo() {
   return await $.getJSON("https://api.ipify.org?format=json", function(data) {
        return data.ip
    })
 }
 

 function ISODateString(d){
    function pad(n){return n<10 ? '0'+n : n}
     
    return pad(d.getUTCMonth()+1)+'/'
    + pad(d.getUTCDate())+' '
    + pad(d.getUTCHours())+':'
    + pad(d.getUTCMinutes())
}


function linWs() {


    let text = `
        Ayuda a ${document.getElementById("user_name").innerHTML} a regalarle un premio a papa!%0Aentra y vota por su gran historia!%0A${url}/final.html?user=${document.getElementById("user_id").innerHTML}
    `
    window.location.href = "http:/api.whatsapp.com/send?text="+text;
}

